---
title: "Guidelines"
date: 2018-12-29T11:02:05+06:00
type: "post"
author: "Somrat"
---


  

Central Pollution Control Board is the nodal agency for publishing of all the guidelines related to Plastic Waste Management in India and EPR. CPCB has played its role by publishing these guidelines from time to time and they can be further grouped in following categories.


### Technical Guidelines

1.[Segregation, Collection and disposal of Plastic Waste:](https://cpcb.nic.in/uploads/plasticwaste/Consolidate_Guidelines_for_disposal_of_PW.pdf)

2.[Guidelines for the Disposal of Non-recyclable Fraction (Multi-layered) Plastic Waste](https://cpcb.nic.in/uploads/plasticwaste/guidelines_nonrecyclable_fraction_24.04.2018.pdf)

3.[Guidelines for Co-processing of Plastic Waste in Cement Kilns](https://cpcb.nic.in/uploads/plasticwaste/Co-processing_Guidelines_Final_23.05.17.pdf)

4.[A Document on Guidelines for Disposal of Thermoset Plastic Waste including Sheet moulding compound (SMC)/Fiber Reinforced Plastic (FRP)](https://cpcb.nic.in/displaypdf.php?id=cGxhc3RpY3dhc3RlL1RoZXJtb3NldF9HdWlkZWxpbmVzLnBkZg==)

### Reporting and compliance guidelines

In a much relief to PIBOs and EPR Agencies, CPCB has come out with a detailed guideline on how to prepare EPR Action Plan, which stakeholders to engage and the timelines. 
[Format for preparation of Action Plan for producers/importers/Brand Owners (PIBOs) under PWM Rules, 2018.](https://cpcb.nic.in/uploads/plasticwaste/Preparation_Action_Plan_PWM_25.06.2019.pdf) 

