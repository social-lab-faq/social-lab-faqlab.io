---
title: "EPR Agency"
date: 2019-08-21T11:02:05+06:00
type: "post"
weight: 2
---

### What is the role of EPR Agency? 
    These entities provide the service of collection, transportation and scientific disposal of plastic waste on behalf of the producers/brand owners/manufacturers. 

### Does it need any authorization from Government to become EPR Agency?
    No. The central pollution control board in this [May 24 notice](https://cpcb.nic.in/uploads/plasticwaste/Notice_PRO_Withdrawal.pdf) has explicitly made it clear that the producer/brand owner/manufacturer can engage any agency of their choice for carrying out EPR activities and derecognized the existing PROs.
    The recent [CPCB guidelines](https://cpcb.nic.in/uploads/plasticwaste/Preparation_Action_Plan_PWM_25.06.2019.pdf) also point towards hiring any EPR Agency for carrying our EPR Activity.

### What is a PRO?
    PRO stands for Producer Responsibility Organization. It was a pan-India recognition given by CPCB to 21 agencies to carry out EPR activities with the purpose to create awareness about EPR in the ecosystem, and lead the process of waste picker integration.