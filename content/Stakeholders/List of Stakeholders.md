---
title: "List of Stakeholders"
date: 2019-08-21T11:02:05+06:00
type: "post"
weight: 1
---

### Government Of India: Ministry of Environment Forests and Climate Change (MoEFCC)
### Central Pollution Control Board (CPCB)
### State Pollution Control Boards (SPCBs)
### Producer
### Importer
### Brand Owner
### Manufacturer
### EPR Agency
### Recycler
### Coprocessor
### Waste Picker
### Scrap Dealer
### Urban Local Body
