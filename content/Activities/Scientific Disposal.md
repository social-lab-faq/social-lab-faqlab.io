---
title: "Scientific Disposal"
date: 2019-08-21T11:02:05+06:00
type: "post"
weight: 1
---

CPCB has from time to time published guidelines regarding scientific disposal of both recyclable and non recyclable Plastic. As per the [Guidelines for the Disposal of Non-recyclable Fraction (Multi-layered) Plastic Waste](https://cpcb.nic.in/uploads/plasticwaste/guidelines_nonrecyclable_fraction_24.04.2018.pdf), following options are available 

  1. Co-processing in Cement kilns

  2. Usage in road construction

  3. Pyrolysis (Waste to Oil) 

### Who can do the Scientific Disposal
Only the entities that hold a valid Consent to Operate from SPCBs or Pollution Control Committees (PCC) in Union Territories.
