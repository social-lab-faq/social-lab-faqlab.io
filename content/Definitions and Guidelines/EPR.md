---
title: "Extended Producer Responsibility (EPR)"
date: 2018-12-29T11:02:05+06:00
type: "post"
author: "Somrat"
---


  

Extended Producer Responsibility (EPR) is definesd in the Plastic Waste Management Rules 2016 as the responsibility of a producer for the environmentally sound management of the product until the end of its life.


EPR as a concept extends to all kinds of waste and is not related exclusively to plastic waste only. It was first introduced in India through the E-Waste Management Rules in 2011.

As per the reports by Central Pollution Control Board, India generates around 25900 tons of Plastic waste per day. And at least 40% of it goes uncollected causing environmental hazards. EPR is expected to help us in solving the problem of plastic waste from collection to scientific disposal in a safe manner.

