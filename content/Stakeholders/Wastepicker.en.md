---
title: "WastePicker"
date: 2019-08-21T11:02:05+06:00
type: "post"
weight: 2
---

Waste pickers are the actual people who are involved in the process of picking up the waste, sorting and segregating it and setting up the first layer of supply chain.

How EPR will benefit the waste-pickers? 
The funding that will flow through the EPR activity will help in integrating and formalizing the waste picker in following ways 
    
    1. Providing personal protection equipment like masks, gloves, jackets 
    
    2. Health insurance 
    
    3. Giving recognition and Identity to the waste pickers.  