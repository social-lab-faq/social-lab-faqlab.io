---
title: "CPCB"
date: 2019-08-21T11:02:05+06:00
type: "post"
weight: 2
---

The role of CPCB is to act as a nodal agency at the national level for formulating guidelines on EPR, scientific disposal mechanisms etc.