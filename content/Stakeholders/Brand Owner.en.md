---
title: "Brand Owner"
date: 2019-08-21T11:02:05+06:00
type: "post"
weight: 2
---

The plastic waste management rules (3)(b) define a Brand owner as a  person  or  company  who sells any  commodity under a registered  brand label.

The CPCB has published following lists of Brand Owners

[Notice to unregistered Brandowners/Producers as per PWM amendment rules, 2018](https://cpcb.nic.in/openpdffile.php?id=UmVwb3J0RmlsZXMvODUyXzE1NTY3OTI3MjBfbWVkaWFwaG90bzcyMi5wZGY=)

[list of accepted Brand-Owners & Producers (as on 14.03.2019)](https://cpcb.nic.in/uploads/plasticwaste/Brand-owners_Producers_list.pdf)