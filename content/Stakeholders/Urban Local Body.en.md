---
title: "Urban Local Body"
date: 2019-08-21T11:02:05+06:00
type: "post"
weight: 2
---

Urban Local Bodies (ULBs) play a crucial role in the entire supply chain of Plastic Waste and EPR.
The PWM Rules 2016 list out various responsibilities to ULBs which include development   and   setting   up   of   infrastructure   for   segregation,   collection,   storage, transportation, processing and disposal of the plastic waste either onits own or by  engaging agencies or producers