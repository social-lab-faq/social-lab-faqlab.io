---
title: "Formalization"
date: 2019-08-21T11:02:05+06:00
type: "post"
Weight: 3
---

Formalization is one of the biggest opportunities because of EPR. Formalization in this context means integrating the informal labour working in the supply chain and giving them the benefits and protection as per the law of the land.

The Waste pickers who work in collection of plastic at various dumping grounds, waste sites etc do so in unhygienic, hazardous conditions without any Protection Equipments like gloves, masks and do not generally have access to life/health insurance benefits.

The funds flowing through EPR are expected to improve the lives of waste pickers and workers involved in the handling of waste.