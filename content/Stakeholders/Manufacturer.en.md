---
title: "Manufacturer"
date: 2019-08-21T11:02:05+06:00
type: "post"
weight: 2
---

As per the PWM Rules 2016 (3)(m),“manufacturer” means and include a person or unit or agency engagedin production of plastic raw material to be used as raw material by the producer. 