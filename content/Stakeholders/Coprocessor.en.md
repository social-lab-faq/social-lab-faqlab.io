---
title: "Coprocessor"
date: 2019-08-21T11:02:05+06:00
type: "post"
weight: 2
---
Coprocessing is a environmentally sound method of disposal of Plastic. CPCB has designated certain cement factories across India to carry out the buring of plastic waste.

The list and guidelines for Coprocessing facilities can be accessed [here](https://cpcb.nic.in/uploads/plasticwaste/Co-processing_Guidelines_Final_23.05.17.pdf) 