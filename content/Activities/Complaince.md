---
title: "Compliance"
date: 2019-08-21T11:02:05+06:00
type: "post"
weight: 2
---

Compliance in case of EPR is regular activity and includes documentation for CPCB as well as respective SPCB/PCC.

Following are the parts of EPR Compliance

1. EPR Action Plan Preparation and Submission to SPCB

2. Quarterly Report to CPCB

3. Documentation of Scientific Disposal

### 1. EPR Action Plan Preparation and Submission to SPCB
This activity must be in accordance with Annexure I of [CPCB guidelines](https://cpcb.nic.in/uploads/plasticwaste/Preparation_Action_Plan_PWM_25.06.2019.pdf).

### 2. Quarterly Report to CPCB
This activity must be in accordance with Annexure II of [CPCB guidelines](https://cpcb.nic.in/uploads/plasticwaste/Preparation_Action_Plan_PWM_25.06.2019.pdf).

### 3. Documentation for Scientific Disposal
This includes a long list of documents that help in verification and validation of the authenticity of scientific disposal of Plastic Waste

  1. Material Photos

  2. Lorry Receipt

  3. E-way Bill

  4. Weight Receipt

  5. Material Acknowledgement Receipt from the recycler/Processor

  6. Recycling/Disposal Certificate.